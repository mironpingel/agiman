<?php

namespace App\Http\Controllers\TimeTracking;

use App\TimeTracking\Timer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class StatsController extends Controller
{
    // Todo - limit to current User

    public function today()
    {
        if (request()->user() === null) { // Should be moved to a middleware
            return response()->json(['error' => 'Unauthorized'],401);
        }

        $todaysTimeStamps = Timer::forCurrentUser()->whereDate('stopped_at', Carbon::today())->get();
        return Timer::addUpTotalTime($todaysTimeStamps);
    }

    public function week()
    {
        if (request()->user() === null) { // Should be moved to a middleware
            return response()->json(['error' => 'Unauthorized'],401);
        }

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $weekTimeStamps = Timer::forCurrentUser()->whereBetween('stopped_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();

        return Timer::addUpTotalTime($weekTimeStamps);
    }

    public function month()
    {
        if (request()->user() === null) { // Should be moved to a middleware
            return response()->json(['error' => 'Unauthorized'],401);
        }

        $monthTimeStamps = Timer::forCurrentUser()->whereBetween('stopped_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->get();
        return Timer::addUpTotalTime($monthTimeStamps);

    }
}
