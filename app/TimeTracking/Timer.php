<?php

namespace App\TimeTracking;

use App\Task\Task;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'started_at',
		'stopped_at',
		'task_id',
		'user_id'
	];

	protected $appends = ['total_time'];

	public function getTotalTimeAttribute()
	{
	    return $this->totalTimeString();
	}

	public function task()
	{
	    return $this->belongsTo(Task::class);
	}

	public function user()
	{
	    return $this->belongsTo(User::class);
	}

	public function scopeForCurrentUser($query, $user = null)
	{
		if ($user === null) {
			$user = request()->user();
		}

	    return $query->where('user_id', $user->id);
	}

	public function scopeRunning($query)
	{
	    return $query->whereNull('stopped_at');
	}

	public function scopeFinished($query)
	{
	    return $query->whereNotNull('stopped_at');
	}

	public function totalTimeString()
	{
	    return $this->totalTime()->format('%H:%I:%S');
	}

	public function totalTime()
	{
		$started_at = new \DateTime($this->started_at);
		$stopped_at = new \DateTime($this->stopped_at);

	    return $stopped_at->diff($started_at);
	}

	public function totalInSeconds()
	{
	    $interval = $this->totalTime();
        return $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
	}

	public static function secondsToString($seconds)
	{
		$hours = floor($seconds / 3600);
		$mins = floor($seconds / 60 % 60);
		$secs = floor($seconds % 60);

		return sprintf('%02d:%02d', $hours, $mins);
	}

	public static function addUpTotalTime($collection)
	{
	    $totalSeconds = 0;

	    foreach ($collection as $time) {
	        $totalSeconds += $time->totalInSeconds();
	    }

	    return Self::secondsToString($totalSeconds);
	}
}
