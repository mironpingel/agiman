<?php

namespace App\Http\Controllers\Task;

use App\Task\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function recent()
    {
        $tasks = Task::orderBy('updated_at', 'desc')->take(5)->get();
        $tasks->each(function($task) {
            $task->setAppends(['project', 'parent']);
        });

        return $tasks;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::create($request->all());
        $task->users()->attach($request->user());

        return $task->load('sub_tasks', 'users');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return $task->load('sub_tasks', 'users');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task->update($request->all());
        $task->project->touch();
        return $task->load('sub_tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
    }
}
