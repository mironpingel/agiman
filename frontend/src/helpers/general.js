import axios from 'axios';


export function setAuthorization(token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`
}

export function initialize(store, router) {
	router.beforeEach((to, from, next) => {
	    const requiresAuth = to.matched.some(route => route.meta.requiresAuth);
	    const { currentUser } = store.state;

	    if(requiresAuth && !currentUser) {
	        next('/login');
	    } else if(to.path === '/login' && currentUser || to.path === '/register' && currentUser) {
	        next('/');
	    } else {
	        next();
	    }
	});

    axios.interceptors.response.use(null, (error) => {
        if (error.response.status === 401) {
            store.commit('logout');
            router.push('/login');
        }

        return Promise.reject(error);
    });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}
