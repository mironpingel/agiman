<?php

use Faker\Generator as Faker;

$factory->define(App\Task\Task::class, function (Faker $faker) {
    return [
    	'name' => $faker->sentence(4),
    	'description' => $faker->paragraph,
    	'closed' => false,
    	'parent_task_id' => 1,
    	'parent_list_id' => 1,
    	'sort_order' => 1,
    ];
});
