<?php

use Faker\Generator as Faker;

$factory->define(App\TaskList\TaskList::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(2),
        'project_id' => 1,
        'sort_order' => 1,
    ];
});
