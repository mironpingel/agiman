import axios from 'axios';

export default {
	state: {

	},

	mutations: {

		/**
			Updates the lists if they are lists reordered (dragged)
		*/
		updateListOrder(state, updatedLists) {
			this.state.lists = updatedLists;
		},

		/**
			Updates the lists if they are lists reordered (dragged)
		*/
		updateListTitle(state, listData) {
			this.state.lists.map(list => {
				if (list.id !== listData.id) {
					return list;
				}
				list.title = listData.input;
				return list;
			})
		},

		addList(state, newList) {
			state.lists.push(newList);
		},

		removeList(state, deletedList) {
			const index = state.lists.indexOf(deletedList);
			state.lists.splice(index, 1);
		},

	},

	actions: {
		async updateListOrder(context, updated) {
			const changes = [];

			updated.lists.forEach((list, index) => {
				changes.push({
					id: list.id,
					sort_order: index
				});
			})

			await axios.post(`/api/projects/${updated.project_id}/order`, changes);

			context.commit("updateListOrder", updated.lists);
		},

		async updateListTitle(context, listData) {
			await axios.post(`/api/lists/${listData.id}`, {
				title: listData.input
			});

			context.commit("updateListTitle", listData);
		},

		async createList(context, list) {
			const { data } = await axios.post(`/api/lists`, {
				sort_order: context.state.lists.length,
				title: list.title,
				project_id: list.project_id
			});

			context.commit("addList", data);
		},

		async deleteList(context, deletedList) {
			const { status } = await axios.delete(`/api/lists/${deletedList.id}`);

			if (status !== 200) {
				return;
			}

			context.commit("removeList", deletedList);
		},
	},
}