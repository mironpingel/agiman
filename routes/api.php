<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    // 'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('register', 'AuthController@create');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'jwt.auth'], function ($router) {

	// Project
	Route::get('projects', 'Project\ProjectController@index');
	Route::get('projects/recent', 'Project\ProjectController@indexRecent');

	Route::get('projects/{project}/time/total', 'Project\TimeController@total');

	Route::get('projects/{project}', 'Project\ProjectController@show');
	Route::delete('projects/{project}', 'Project\ProjectController@destroy');
	Route::post('projects', 'Project\ProjectController@store');
	Route::post('projects/{project}', 'Project\ProjectController@update');
	Route::get('projects/{project}/lists', 'Project\ProjectListController@index');
	Route::post('projects/{project}/order', 'Project\OrderController@update');

	// Task
	Route::get('tasks/recent', 'Task\TaskController@recent');
	Route::get('tasks/{task}', 'Task\TaskController@show');
	Route::post('tasks/{task}', 'Task\TaskController@update');
	Route::post('tasks', 'Task\TaskController@store');
	Route::delete('tasks/{task}', 'Task\TaskController@destroy');

	// Task / User
	Route::post('tasks/{task}/users/{user}/attach', 'Task\UserController@attach');
	Route::post('tasks/{task}/users/{user}/detach', 'Task\UserController@detach');

	// User / Tasks
	Route::get('users/tasks', 'User\TaskController@index');
	Route::get('users/tasks/recent', 'User\TaskController@indexLatest');

	// User
	Route::get('users', 'User\UserController@index');
	Route::patch('users/{user}', 'User\UserController@update');

	// List
	Route::post('lists', 'TaskList\TaskListController@store');
	Route::delete('lists/{taskList}', 'TaskList\TaskListController@destroy');
	Route::post('lists/{taskList}', 'TaskList\TaskListController@update');
	Route::post('lists/{taskList}/order', 'TaskList\OrderController@update');

	// Timer
	Route::get('users/{user}/timers', 'TimeTracking\UserController@index');
	Route::get('users/{user}/timers/active', 'TimeTracking\UserController@active');

	Route::post('users/{user}/timers', 'TimeTracking\TimerController@store');
	Route::post('users/{user}/timers/{timer}/stop', 'TimeTracking\TimerController@stop');
	Route::post('users/{user}/timers/{timer}', 'TimeTracking\TimerController@update');
	Route::delete('users/{user}/timers/{timer}', 'TimeTracking\TimerController@destroy');

	// Time
	Route::get('time/today', 'TimeTracking\StatsController@today');
	Route::get('time/week', 'TimeTracking\StatsController@week');
	Route::get('time/month', 'TimeTracking\StatsController@month');

	// Search
	Route::get('search/all', 'Search\SearchController@all');
});