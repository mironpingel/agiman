import axios from 'axios';

export default {
	state: {
		showGuidedTour: false,
		currentTooltipIndex: 0,
		tooltipLocation: "/",
		tooltips: [
			{
				name: 'placeholder',
				location: '/'
			},
			{
				name: 'timerTooltip1',
				location: '/'
			},
			{
				name: 'searchTooltip1',
				location: '/'
			},
			{
				name: 'navbarTooltip1',
				location: '/'
			},
			{
				name: 'expandNavbarTooltip',
				location: '/'
			},
		]
	},

	getters: {
		currentTooltip(state) {
			return state.tooltips[state.currentTooltipIndex];
		}
	},

	mutations: {
		nextTooltip(state) {
			if (state.currentTooltipIndex === state.tooltips.length) {
				state.currentTooltipIndex = state.tooltips.length;
			} else {
				state.currentTooltipIndex = state.currentTooltipIndex + 1;
			}
		},

		previousTooltip(state) {
			if (state.currentTooltipIndex === 0) {
				state.currentTooltipIndex;
			} else {
				state.currentTooltipIndex = state.currentTooltipIndex - 1;
			}
		},

		toggleDisplayOfTooltips(state) {
			state.showTooltips = !state.showTooltips;
		},

		completeTooltips(state) {
			state.currentTooltipIndex = 0;
			state.showGuidedTour = false;
		},
	},

	actions: {
		async completeTour(context) {
			const { status } = await axios.patch(`/api/users/${context.state.currentUser.id}`,{
				'show_tooltips' : false
			});

			if (status === 200) {
				context.commit('completeTooltips');
			}
		},
	},
}