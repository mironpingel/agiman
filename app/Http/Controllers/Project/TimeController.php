<?php

namespace App\Http\Controllers\Project;

use App\Project\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeController extends Controller
{
    public function total(Project $project)
    {
        $totalSeconds = $project->totalTimeTracked();

        return $totalSeconds;
    }
}
