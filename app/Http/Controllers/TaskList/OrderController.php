<?php

namespace App\Http\Controllers\TaskList;

use App\Http\Controllers\Controller;
use App\TaskList\TaskList;
use App\Task\Task;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Update the sort_order for each task in the list
     * Also update the parent_list_id in case of the task was moved to another list
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskList\TaskList  $taskList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskList $taskList)
    {
        $changes = $request->all();

        foreach ($changes as $task) {
            Task::find($task["id"])->update([
                "parent_list_id" => $task["parent_list_id"],
                "sort_order" => $task["sort_order"]
            ]);
        }

        return response()->json(['success' => 'success'], 200);
    }
}
