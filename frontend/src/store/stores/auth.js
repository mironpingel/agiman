import axios from 'axios';
import { getLocalUser, login } from "../../helpers/auth";


const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        customers: []
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        },
        customers(state) {
            return state.customers;
        }
    },
    mutations: {
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },

        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
			localStorage.setItem("user", JSON.stringify(state.currentUser));

            if (payload.user.show_tooltips) {
            	state.showGuidedTour = true;
            }
        },

        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.e;
        },

        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
            state.showGuidedTour = false;
        },
    },
    actions: {
        async login(context, userCredentials) {
            context.commit("login");

            try {
                const { data } = await login(userCredentials);
                context.commit("loginSuccess", data);
            } catch(e) {
                context.commit("loginFailed", {e});
            }
        },

        async logout(context) {
            await axios.post('/api/auth/logout');
            context.commit("logout");
        }
    }
};