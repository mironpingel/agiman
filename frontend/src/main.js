import 'vue-flash-message/dist/vue-flash-message.min.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import PrettyCheck from 'pretty-checkbox-vue/check';
import 'pretty-checkbox/src/pretty-checkbox.scss';


// eslint-disable-next-line
import swal from 'sweetalert';
import VueFlashMessage from 'vue-flash-message';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faFolderOpen, faAngleRight, faAngleLeft, faTimes, faStopwatch, faSignOutAlt, faEllipsisH, faListUl, faPlus, faCog, faUsers, faClock, faPen, faCheck, faExchangeAlt, faStop, faPlay, faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { Modal, Popover } from 'bootstrap-vue/es/components';
import VueFuse from 'vue-fuse'

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';
import { initialize } from './helpers/general';


Vue.use(VueFuse)
Vue.use(Modal);
Vue.use(Popover);
Vue.use(VueFlashMessage, {
  messageOptions: {
    timeout: 3000,
  }
});


Vue.component('checkbox', PrettyCheck);
library.add(faHome, faFolderOpen, faAngleRight, faAngleLeft, faTimes, faStopwatch, faSignOutAlt, faEllipsisH, faListUl, faPlus, faCog, faUsers, faClock, faPen, faCheck, faExchangeAlt, faStop, faPlay, faSearch );
Vue.component('icon', FontAwesomeIcon)



initialize(store, router);
Vue.config.productionTip = false;

const app = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

// Global Keyboard events
document.addEventListener('keyup', (event) => {
	switch (event.key) {

		case 'Escape':
			if (app.$store.state.slideInIsOpen) {
				app.$store.commit('closeSlideIn');
			}
			break;

		case 'F':
			if (event.shiftKey && event.ctrlKey) {
				app.$children
					.find(child => child.$el.id === 'app').$children
					.find(child => child.$el.id === 'topbar')
					.$refs.search_field.focus()
			}
			break;

		case 'T':
			if (event.shiftKey && event.ctrlKey) {
				if (app.$store.state.runningTimer) {
					app.$store.dispatch('stopTimer');
					break;
				}

				app.$store.dispatch('startTimer');
			}
			break;

		default:
			// console.log(event);
			break;

	}
})