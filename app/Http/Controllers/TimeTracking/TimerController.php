<?php

namespace App\Http\Controllers\TimeTracking;

use App\Http\Controllers\Controller;
use App\TimeTracking\Timer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TimerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        if ($user === null) {
            $user = request()->user();
        }

        $timer = Timer::create([
                'started_at' => new Carbon,
                'task_id' => $request->input('task_id'),
                'user_id' => $user->id,
            ]);

        return $timer;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeTracking\Timer  $timer
     * @return \Illuminate\Http\Response
     */
    public function stop(Request $request, User $user, Timer $timer)
    {
        $timer->update([
            'stopped_at' => (new Carbon)->format('Y-m-d H:i:s'),
        ]);

        $timer->total = $timer->totalTimeString();

        if ($task = $timer->task) {
            $task->project->touch();
            $timer->load('task');
            $timer->task->setAppends(['parent', 'project', 'is_sub_task']);
        }

        return $timer;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeTracking\Timer  $timer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Timer $timer)
    {
        $timer->update($request->all());
        $timer->total = $timer->totalTimeString();
        $timer->load('task');

        if ($timer->task) {
            $timer->task->setAppends(['project', 'parent']);
        }

        return $timer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeTracking\Timer  $timer
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Timer $timer)
    {
        $timer->delete();
    }
}
