<?php

namespace App\Http\Controllers\Project;

use App\Project\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $projects->each(function($project) {
            $project->setAppends(["open_tasks", "closed_tasks", "open_user_tasks", "closed_user_tasks"]);
        });

        return $projects;
    }

    /**
     * Display a listing of the 5 most recently edited projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRecent()
    {
        return Project::orderBy('updated_at', 'desc')->take(7)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Project::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return $project;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $project->update($request->all());
        return $project;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->lists->each(function ($list) {
            $list->tasks->each(function ($task) {
                $task->delete();
            });

            $list->delete();
        });

        $project->delete();
    }
}
