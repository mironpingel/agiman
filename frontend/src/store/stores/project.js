import axios from 'axios';

export default {
	state: {
		projectBeingEdited: {},
	},

	mutations: {
		openProjectForm(state, project = null) {
			if (!project) {
				project = {};
			}

			state.projectBeingEdited = project;
			state.slideInContentType = 'project';
			state.slideInIsOpen = true;
		},

		updateProject(state, updatedProject) {
			state.projects = state.projects.map(project => {
				if (project.id === updatedProject.id) {
					return updatedProject;
				}

				return project;
			})
		},

		/**
			Adds a new Project to its parent list
		*/
		addProject(state, newProject) {
			if (state.projects.findIndex(project => project.id === newProject.id) !== -1) {
				console.log(state.projects)
				return;
			}

			state.projects.unshift(newProject);
		},

		setProjectLists(state, lists) {
			state.lists = lists;
		},

		removeProject(state, projectId) {
			const index = state.projects.findIndex(project => project.id === Number(projectId));

			if (index !== -1) {
				state.projects.splice(index, 1);
			}
		},

		resetProjects(state) {
			state.projects = [];
		},
	},

	actions: {
		async openProjectForm(context, project) {
			context.commit("openProjectForm", project);
		},

		async updateProject(context, project) {
			const { data } = await axios.post(`/api/projects/${project.id}`, {
				name: project.name,
				client_id: 1, // For now
				pinned: false,
				description: project.description
			});

			context.commit("updateProject", data);
		},

		async createProject(context, project) {
			const { data } = await axios.post(`/api/projects`, {
				name: project.name,
				client_id: 1, // For now
				pinned: false,
				desription: project.description
			});

			context.commit("addProject", data);
		},

		async fetchProjectLists(context, projectId) {
			const { data } = await axios.get(`/api/projects/${projectId}/lists`);

			context.commit('setProjectLists', data);
		},

		async fetchProjects(context, filters = null) {
			if (filters) {
				// do stuff?
			}

			const { data } = await axios.get(`/api/projects`);

			context.commit('resetProjects');

			data.forEach(project => {
				context.commit('addProject', project);
			})
		},

		async deleteProject(context, projectId) {
			const { status } = await axios.delete(`/api/projects/${projectId}`);

			if (status !== 200) {
				return;
			}

			context.commit('removeProject', projectId);
		},


	},
}