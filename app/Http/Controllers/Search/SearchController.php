<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Project\Project;
use App\Task\Task;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function all()
    {
    	$tasks = [];
        foreach (Task::all() as $task) {
        	array_push($tasks, [
        		"type" => "task",
        		"name" => $task->name,
        		"project_id" => ($task->project) ? $task->project->id : null,
        		"task_id" => $task->id
        	]);
        }

        $projects = [];
        foreach (Project::all() as $project) {
        	array_push($projects, [
        		"type" => "project",
        		"project_id" => $project->id,
        		"name" => $project->name
        	]);
        }

        return array_flatten([$tasks, $projects], 1);
    }
}
