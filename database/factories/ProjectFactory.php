<?php

use Faker\Generator as Faker;

$factory->define(App\Project\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(3),
        'description' => $faker->paragraph,
        'pinned' => $faker->boolean,
    ];
});
