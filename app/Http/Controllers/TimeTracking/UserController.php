<?php

namespace App\Http\Controllers\TimeTracking;

use App\Http\Controllers\Controller;
use App\TimeTracking\Timer;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of all finished timers for a user
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $timers = Timer::finished()
                    ->forCurrentUser($user)
                    ->get()
                    ->sortBy('stopped_at');

        $timers->each(function($timer) {
            $timer->load('task');

            if ($timer->task) {
                $timer->task->setAppends(['parent', 'project', 'is_sub_task']);
            }
        });

        return array_reverse($timers->toArray());
    }

    /**
        Returns the latest running timer for a given user
    */

    public function active(User $user)
    {
        return Timer::forCurrentUser($user)->where('stopped_at', null)->orderBy('started_at', 'DESC')->first();
    }


}
