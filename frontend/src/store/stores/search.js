import axios from 'axios';

export default {
	state: {
		searchResults: []
	},

	getters: {

	},

	mutations: {
		updateSearchResults(state, newSet) {
			state.searchResults = newSet;
		},
	},

	actions: {

	},
}