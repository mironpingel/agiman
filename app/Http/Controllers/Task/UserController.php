<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Task\Task;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function attach(Task $task, User $user)
    {
        $task->users()->attach($user);

        return $task->load('sub_tasks', 'users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function detach(Task $task, User $user)
    {
        $task->users()->detach($user);

        return $task->load('sub_tasks', 'users');
    }
}
