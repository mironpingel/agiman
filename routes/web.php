<?php

use App\Project\Project;
use App\TaskList\TaskList;
use App\Task\Task;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function() {
	return Project::all();
});

// Inside the web.php file
Route::get('/{any}', 'SpaController@index')->where('any', '.*');