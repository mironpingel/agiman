import axios from 'axios';

export default {
	state: {
		taskBeingEdited: {}
	},

	mutations: {

		/**
			Updates the relevant lists, when a task is dragged to a new position
		*/
		updateTasks(state, newData) {
			state.lists.map(list => {
				if (list.id === newData.listId) {
					list.tasks = newData.tasks.map(task => {
						// Update the list ID on the tasks
						task.parent_list_id = newData.listId;
						return task;
					});
				}
				return list;
			})
		},


		/**
			Locates the right task on the relevant list and toggles the closed status
		*/
		toggleTaskStatus(state, updatedTask) {
			state.lists.map(list => {
				if (list.id !== updatedTask.parent_list_id) {
					return list;
				}

				list.tasks.map(task => {
					if (task.id !== updatedTask.id) {
						return task;
					}

					task.closed = !task.closed;
					return updatedTask;
				});

				return list;
			})
		},


		/**
			Locates the right sub-task on the relevant task and toggles the closed status
		*/
		toggleSubTaskStatus(state, updatedTask) {
			state.lists.map(list => {
				list.tasks = list.tasks.map(task => {
					if (task.id !== updatedTask.parent_task_id) {
						return task;
					}

					task.sub_tasks.map(subTask => {
						if (subTask.id === updatedTask.id) {
							subTask.closed = !subTask.closed;
						}
						return subTask;
					})

					return task;
				});

				return list;
			})
		},


		/**
			Adds a new task to its parent list
		*/
		addTask(state, newTask) {
			state.lists.map(list => {
				if (list.id !== newTask.parent_list_id) {
					return list;
				}

				list.tasks.unshift(newTask);

				return list;
			})
		},

		/**
			Adds a new task to its parent list
		*/
		selectTaskToEdit(state, taskToEdit) {
			state.taskBeingEdited = taskToEdit;
			state.slideInContentType = 'task';
			state.slideInIsOpen = true;
		},


		/**
			Closes the Slide-in
		*/
		updateTask(state, updatedTask) {
			state.lists = state.lists.map(list => { // --TODO Refactor, move this find task on list logic to helper function

				if (list.id === updatedTask.parent_list_id) {
					list.tasks = list.tasks.map(task => {
						if (task.id === updatedTask.id) {
							if (state.taskBeingEdited.id === updatedTask.id) { // Ugly fix
								state.taskBeingEdited = updatedTask;
							}

							return updatedTask;
						}

						return task;
					})
				}
				return list;
			})
		},


		/**
			Create Sub-Task on existing Task
		*/
		addSubTask(state, newTask) {
			state.lists.map(list => {
				list.tasks.map(task => {
					if (task.id === newTask.parent_task_id) {
						if (!task.sub_tasks) {
							task.sub_tasks = [
								newTask
							];
						}

						task.sub_tasks.unshift(newTask);
						state.taskBeingEdited = task; // Update the edit view
					}

					return task;
				})

				return list;
			})
		},


		/**
			Removes a task from its parent
		*/
		removeTask(state, deletedTask) {
			let foundTask = false;

			state.lists.map(list => {

				// Look for the deletedTask in top-level tasks
				list.tasks.forEach((task, index) => {
					if (task.id !== deletedTask.id) {
						return;
					}

					foundTask = true;
					list.tasks.splice(index, 1);
				})

				// If no top-level match was found, then check sub-tasks
				if (!foundTask) {
					list.tasks.forEach(task => {
						task.sub_tasks.forEach((subTask, index) => {
							if (subTask.id !== deletedTask.id) {
								return;
							}

							task.sub_tasks.splice(index, 1);
							console.log(task);
							state.taskBeingEdited = task; // Update the edit view
						})
					})
				}

				return list;
			});

		},


	},

	actions: {
		async updateTasks(context, newData) {
			const tasks = newData.tasks.map(task => task); // Copy the array
			tasks.reverse();

			const changes = tasks.map((task, index) => ({
				id: task.id,
				parent_list_id: newData.listId,
				sort_order: index
			}));

			await axios.post(`/api/lists/${newData.listId}/order`, changes);

			context.commit("updateTasks", newData);
		},

		async toggleTaskStatus(context, task) {
			await axios.post(`/api/tasks/${task.id}`, {
				closed: !task.closed
			});

			context.commit("toggleTaskStatus", task);
		},

		async toggleSubTaskStatus(context, task) {
			await axios.post(`/api/tasks/${task.id}`, {
				closed: !task.closed
			});

			context.commit("toggleSubTaskStatus", task);
		},

		async createTask(context, task) {
			const { data } = await axios.post(`/api/tasks`, {
				...task
			});

			context.commit("addTask", data);
		},

		async createSubTask(context, task) {
			const { data } = await axios.post(`/api/tasks`, {
				...task
			});

			context.commit("addSubTask", data);
		},

		async selectTaskToEdit(context, task) {
			const { data } = await axios.get(`/api/tasks/${task.id}`);
			context.commit("selectTaskToEdit", data);
		},

		async updateTask(context, task) {
			const { data } = await axios.post(`/api/tasks/${task.id}`, {
				...task
			});

			context.commit("updateTask", data);
		},

		async deleteTask(context, task) {
			const { status } = await axios.delete(`/api/tasks/${task.id}`);

			if (status !== 200) {
				return;
			}

			context.commit("removeTask", task);
		},

	},
}