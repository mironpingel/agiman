<?php

namespace App\Project;

use App\TaskList\TaskList;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
    	'name',
    	'description',
    	'client_id',
    	'pinned'
    ];

    public function lists()
    {
    	return $this->hasMany(TaskList::class);
    }

    public function getOpenTasksAttribute()
    {
        $count = 0;
        foreach ($this->lists as $list) {
            $count += $list->tasks()->where("closed", 0)->get()->count();
        }

        return $count;
    }

    public function getClosedTasksAttribute()
    {
        $count = 0;
        foreach ($this->lists as $list) {
            $count += $list->tasks()->where("closed", 1)->get()->count();
        }

        return $count;
    }

    public function getOpenUserTasksAttribute()
    {
        $count = 0;

        foreach ($this->lists as $list) {
            $tasks = $list->tasks()
                ->where("closed", 0)
                ->get();

            foreach ($tasks as $task) {
                if ($task->users->contains(request()->user())) {
                    $count++;
                }
            }
        }

        return $count;
    }

    public function getClosedUserTasksAttribute()
    {
        $count = 0;

        foreach ($this->lists as $list) {
            $tasks = $list->tasks()
                ->where("closed", 1)
                ->get();

            foreach ($tasks as $task) {
                if ($task->users->contains(request()->user())) {
                    $count++;
                }
            }
        }

        return $count;
    }

    public function totalTimeTracked()
    {
        $totalSeconds = 0;

        if (!$this->lists) {
            return;
        }

        foreach ($this->lists as $list) {
            if (!$list->tasks) {
                continue;
            }

            foreach ($list->tasks as $task) {
                $totalSeconds += $task->totalSecondsTracked();
            }
        }

        return $totalSeconds;
    }
}
