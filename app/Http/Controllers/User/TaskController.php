<?php

namespace App\Http\Controllers\User;

use App\Task\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->user() === null) { // Should be moved to a middleware
            return response()->json(['error' => 'Unauthorized'],401);
        }

        return $request->user()->tasks;
    }

    /**
     * Display the last 5 updated tasks for the user
     *
     * @return \Illuminate\Http\Response
     */
    public function indexLatest(Request $request)
    {
        if (request()->user() === null) { // Should be moved to a middleware
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $tasks = $request->user()->tasks()->orderBy('updated_at', 'desc')->take(6)->get();
        $tasks->each(function($task) {
            $task->setAppends(['project', 'parent']);
        });

        return $tasks;
    }
}
