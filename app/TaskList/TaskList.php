<?php

namespace App\TaskList;

use App\Project\Project;
use App\Task\Task;
use Illuminate\Database\Eloquent\Model;

class TaskList extends Model
{
    protected $fillable = [
    	'id',
		'title',
		'sort_order',
		'project_id'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'parent_list_id');
    }
}
