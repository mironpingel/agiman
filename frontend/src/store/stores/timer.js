import axios from 'axios';

export default {
	state: {
		timerState: "",
		startTime: Date.now(),
		currentTime: Date.now(),
		runningTimer: null,
		times: []
	},

	mutations: {
		setStartTime(state, time) {
			state.startTime = time;
		},

		setCurrentTime(state, time) {
			state.currentTime = time;
		},

		setTimerState(state, timerState) {
			state.timerState = timerState;
		},

		setRunningTimer(state, timerId) {
			state.runningTimer = timerId;
		},

		resetRunningTimer(state) {
			state.runningTimer = null;
		},

		storeTimesForCurrentUser(state, times) {
			state.times = times;
		},

		addTimeStamp(state, timeStamp) {
			state.times.unshift(timeStamp);
		},

		updateTimeStamp(state, updatedTimeStamp) {
			state.times = state.times.map(timeStamp => {
				if (timeStamp.id !== updatedTimeStamp.id) {
					return timeStamp;
				}

				return updatedTimeStamp;
			})
		},

		removeTimeStamp(state, timeStampId) {
			const index = state.times.findIndex(time => time.id === timeStampId);
			state.times.splice(index, 1);
		},
	},







	actions: {
		async startTimer(context, taskId = null) {
			if (context.state.runningTimer !== null) {
				await context.dispatch('stopTimer');
			}

			const { data } = await axios.post(`/api/users/${context.state.currentUser.id}/timers`, {
				task_id: taskId
			});

			context.commit('setTimerState', "started");
			context.commit('setStartTime', Date.now());
			context.commit('setCurrentTime', Date.now());
			context.commit('setRunningTimer', data.id);
		},

		async resumeActiveTimer(context) {
			const { data } = await axios.get(`/api/users/${context.state.currentUser.id}/timers/active`);

			if (!data) {
				return;
			}

			context.commit('setTimerState', "started");
			context.commit('setStartTime', Date.parse(data.started_at));
			context.commit('setCurrentTime', Date.now());
			context.commit('setRunningTimer', data.id);
		},

		async stopTimer(context) {
			const { data } = await axios.post(`/api/users/${context.state.currentUser.id}/timers/${context.state.runningTimer}/stop`);

			const now = Date.now();
            context.commit('setTimerState', "stopped");
            context.commit('setStartTime', now);
            context.commit('setCurrentTime', now);
            context.commit('resetRunningTimer');

            context.commit('addTimeStamp', data);
		},

		async fetchTimesForCurrentUser(context) {
			const { data } = await axios.get(`/api/users/${context.state.currentUser.id}/timers`);

			context.commit('storeTimesForCurrentUser', data);
		},

		async updateTimeStamp(context, updates) {
			const { data } = await axios.post(`/api/users/${context.state.currentUser.id}/timers/${updates.id}`, {
				started_at: updates.start,
				stopped_at: updates.end
			});

			context.commit('updateTimeStamp', data);
		},

		async deleteTimeStamp(context, timeStampId) {
			const { status } = await axios.delete(`/api/users/${context.state.currentUser.id}/timers/${timeStampId}`);

			if (status !== 200) {
				return;
			}

			context.commit('removeTimeStamp', timeStampId);

		},


	},
}