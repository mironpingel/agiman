import Vue from 'vue';
import Vuex from 'vuex';

import auth from './stores/auth';
import project from './stores/project';
import timer from './stores/timer';
import list from './stores/list';
import task from './stores/task';
import search from './stores/search';
import tooltip from './stores/tooltip';

import clients from './mockdata/clients';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		...auth.state,
		...project.state,
		...list.state,
		...task.state,
		...timer.state,
		...search.state,
		...tooltip.state,

		lists: [],
		projects: [],
		clients,
		slideInContentType: null,
		navbarIsExpanded: false,
		slideInIsOpen: false,
		showMoveModal: false,
	},

	getters: {
		...auth.getters,
		...search.getters,
		...tooltip.getters
	},

	mutations: {
		...auth.mutations,
		...project.mutations,
		...list.mutations,
		...task.mutations,
		...timer.mutations,
		...search.mutations,
		...tooltip.mutations,


		/**
			Toggles the navbar (expanded or not expanded that is the question)
		*/
		toggleNavbar(state) {
			state.navbarIsExpanded = !state.navbarIsExpanded;
		},

		toggleMoveModal(state) {
			state.showMoveModal = !state.showMoveModal;
		},

		/**
			Closes the Slide-in
		*/
		closeSlideIn(state) {
			state.slideInIsOpen = false;
		},
	},

	actions: {
		...auth.actions,
		...project.actions,
		...list.actions,
		...task.actions,
		...timer.actions,
		...search.actions,
		...tooltip.actions,


		async closeSlideIn(context) {
			context.commit("closeSlideIn");
		},
	}
});

export default store;


