<?php

namespace App\Task;

use App\TaskList\TaskList;
use App\TimeTracking\Timer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
    	'name',
    	'is_sub_task',
    	'description',
    	'closed',
    	'parent_list_id',
    	'parent_task_id',
    	'sort_order'
    ];

    protected $appends = ['is_sub_task'];

    public function list()
    {
        return $this->belongsTo(TaskList::class, 'parent_list_id');
    }

    public function parent_task()
    {
        return $this->belongsTo(Task::class, 'parent_task_id');
    }

    public function sub_tasks()
    {
        return $this->hasMany(Task::class, 'parent_task_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'task_users');
    }

    public function timers()
    {
        return $this->hasMany(Timer::class);
    }

    public function getParentAttribute()
    {
        if ($this->parent_task) {
            return $this->parent_task;
        }

        return $this->list;
    }

    public function getIsSubTaskAttribute()
    {
        if ($this->parent_task) {
            return true;
        }

        return false;
    }

    public function totalSecondsTracked()
    {
        $totalSeconds = 0;
        if ($this->timers) {
            foreach ($this->timers as $timer) {
                $totalSeconds += $timer->totalInSeconds();
            }
        }

        return $totalSeconds;
    }

    public function getProjectAttribute()
    {
        $task = $this;

        if ($this->parent_task !== null) {
            $task = $this->parent_task;
        }

        return $task->list->project ?? null;
    }
}
