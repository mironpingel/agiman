import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('./components/Auth/Login.vue'),
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('./components/Auth/Register.vue'),
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
          requiresAuth: true
      }
    },
{
  path: '/projects',
  // props: true,
  name: 'project-index',
  component: () => import('./views/ProjectIndex.vue'),
  meta: {
      requiresAuth: true
  }
},
    {
      path: '/projects/:id',
      // props: true,
      name: 'project',
      component: () => import('./views/Project.vue'),
      meta: {
          requiresAuth: true
      }
    },
    {
      path: '/projects/:id/task/:taskid',
      // props: true,
      name: 'task',
      component: () => import('./views/Project.vue'),
      meta: {
          requiresAuth: true
      }
    },
    {
      path: '/tracked-time',
      // props: true,
      name: 'tracked-time',
      component: () => import('./views/TrackedTime.vue'),
      meta: {
          requiresAuth: true
      }
    },
  ],
});
