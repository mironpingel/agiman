<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Project\Project;
use App\TaskList\TaskList;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Update the order of the lists contained in the project
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $changes = $request->all();

        foreach ($changes as $list) {
            TaskList::find($list["id"])->update([
                "sort_order" => $list["sort_order"]
            ]);
        }

        return response()->json(['success' => 'success'], 200);
    }
}
